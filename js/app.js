// Importar las funciones necesarias del SDK
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.11.1/firebase-app.js";
import { getDatabase, onValue, ref, set, get, update, child, remove } from "https://www.gstatic.com/firebasejs/10.11.1/firebase-database.js";

// Configuración de Firebase
const firebaseConfig = {
    apiKey: "AIzaSyBBBHVJ41WMfh_14xC-LY-ckwSD46m38Tc",
    authDomain: "practicapw-a4d71.firebaseapp.com",
    databaseURL: "https://practicapw-a4d71-default-rtdb.firebaseio.com",
    projectId: "practicapw-a4d71",
    storageBucket: "practicapw-a4d71.appspot.com",
    messagingSenderId: "948217278285",
    appId: "1:948217278285:web:ebb227fdb6e95918135104"
};

// Inicializar Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);

// Declarar variables globales
let numSerie = "";
let marca = "";
let modelo = "";
let descripcion = "";
let urlImg = "";

// Funciones
function leerInputs() {
    numSerie = document.getElementById('txtNumSerie').value.trim();
    marca = document.getElementById('txtMarca').value.trim();
    modelo = document.getElementById('txtModelo').value.trim();
    descripcion = document.getElementById('txtDescripcion').value.trim();
    urlImg = document.getElementById('txtUrl').value.trim();
}

function mostrarMensaje(mensaje) {
    const mensajeElement = document.getElementById('mensaje');
    mensajeElement.textContent = mensaje;
    mensajeElement.style.display = 'block';
    setTimeout(() => {
        mensajeElement.style.display = 'none';
    }, 3000);
}

function limpiarInputs() {
    document.getElementById('txtNumSerie').value = '';
    document.getElementById('txtMarca').value = '';
    document.getElementById('txtModelo').value = '';
    document.getElementById('txtDescripcion').value = ''; 
    document.getElementById('txtUrl').value = '';
}

function escribirInputs() {
    document.getElementById('txtMarca').value = marca;
    document.getElementById('txtModelo').value = modelo;
    document.getElementById('txtDescripcion').value = descripcion;
    document.getElementById('txtUrl').value = urlImg;
}

function insertarProducto() {
    leerInputs();
    if (numSerie === "" || marca === "" || modelo === "" || descripcion === "") {
        mostrarMensaje("Faltaron datos por capturar.");
        return;
    }

    get(child(ref(db), `Automoviles/${numSerie}`)).then((snapshot) => {
        if (snapshot.exists()) {
            mostrarMensaje("El producto ya existe. Puedes actualizarlo si es necesario.");
        } else {
            set(ref(db, `Automoviles/${numSerie}`), {
                numSerie: numSerie,
                marca: marca,
                modelo: modelo,
                descripcion: descripcion,
                urlImg: urlImg
            }).then(() => {
                mostrarMensaje("Se agregó con éxito.");
                limpiarInputs();
                listarProductos();
            }).catch((error) => {
                mostrarMensaje("Ocurrió un error: " + error);
            });
        }
    });
}

function listarProductos() {
    const dbRef = ref(db, 'Automoviles');
    const tabla = document.getElementById('tablaProductos').getElementsByTagName('tbody')[0];
    tabla.innerHTML = '';

    onValue(dbRef, (snapshot) => {
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const data = childSnapshot.val();
            
            const fila = document.createElement('tr');
            fila.innerHTML = `
                <td>${childKey}</td>
                <td>${data.marca}</td>
                <td>${data.modelo}</td>
                <td>${data.descripcion}</td>
                <td><img src="${data.urlImg}" width="100" /></td>
            `;
            tabla.appendChild(fila);
        });
    });
}

function buscarAutomoviles() {
    numSerie = document.getElementById('txtNumSerie').value.trim();
    if (numSerie === "") {
        mostrarMensaje("Falto capturar el número de serie.");
        return;
    }
    
    get(child(ref(db), `Automoviles/${numSerie}`)).then((snapshot) => {
        if (snapshot.exists()) {
            const data = snapshot.val();
            marca = data.marca;
            modelo = data.modelo;
            descripcion = data.descripcion;
            urlImg = data.urlImg;
            escribirInputs();
        } else {
            limpiarInputs();
            mostrarMensaje("No se encontró el registro.");
        }
    });
}

function actualizarAutomovil() {
    leerInputs();
    if (numSerie === "" || marca === "" || modelo === "" || descripcion === "") {
        mostrarMensaje("Favor de capturar toda la información.");
        return;
    }

    update(ref(db, `Automoviles/${numSerie}`), {
        numSerie: numSerie,
        marca: marca,
        modelo: modelo,
        descripcion: descripcion,
        urlImg: urlImg
    }).then(() => {
        mostrarMensaje("Se actualizó con éxito.");
        limpiarInputs();
        listarProductos();
    }).catch((error) => {
        mostrarMensaje("Ocurrió un error: " + error);
    });
}

function eliminarAutomovil() {
    numSerie = document.getElementById('txtNumSerie').value.trim();
    if (numSerie === "") {
        mostrarMensaje("No se ingresó un número de serie válido.");
        return;
    }

    get(child(ref(db), `Automoviles/${numSerie}`)).then((snapshot) => {
        if (snapshot.exists()) {
            remove(ref(db, `Automoviles/${numSerie}`)).then(() => {
                mostrarMensaje("Producto eliminado con éxito.");
                limpiarInputs();
                listarProductos();
            }).catch((error) => {
                mostrarMensaje("Ocurrió un error al eliminar el producto: " + error);
            });
        } else {
            limpiarInputs();
            mostrarMensaje("El producto con número de serie " + numSerie + " no existe.");
        }
    });
}

// Asignar eventos a los botones
document.getElementById('btnAgregar').addEventListener('click', insertarProducto);
document.getElementById('btnBuscar').addEventListener('click', buscarAutomoviles);
document.getElementById('btnActualizar').addEventListener('click', actualizarAutomovil);
document.getElementById('btnBorrar').addEventListener('click', eliminarAutomovil);

// Inicializar la tabla de productos al cargar la página
listarProductos();
